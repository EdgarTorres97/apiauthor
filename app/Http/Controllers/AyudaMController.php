<?php

namespace App\Http\Controllers;

use App\AyudaM;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;



class AyudaMController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    
    /**
     * List the Centres.
     *
     * @return void
     */
    public function index()
    {
        $centre = AyudaM::all();
        return $this->successResponse($centre);
    }

    
    /**
     * Create a Centre.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|max:100',
            'latitud' => 'required|max:80',
            'longitud' => 'required|max:80',
            'estado' => 'required|max:100',
            'ciudad' => 'required|max:100',
            'horario' => 'required|max:100',
            'especialidad' => 'required|max:100',
            'tipo' => 'required|max:100',
            'cpp' => 'required|max:100',
            'numero' => 'max:100'
        ];
        $this->validate($request, $rules);

        $centre = AyudaM::create($request->all());

        $this->successResponse($centre, Response::HTTP_CREATED);
    }
    
    /**
     * Show a Centre.
     *
     * @return void
     */
    public function show($centre)
    {
        $centre = AyudaM::findOrfail($centre);
        return $this->successResponse($centre);

    }
    
    /**
     * Update a Centre.
     *
     * @return void
     */
    public function Update(Request $request, $centre)
    {
        $rules = [
            'nombre' => 'required|max:100',
            'latitud' => 'required|max:80',
            'longitud' => 'required|max:80',
            'estado' => 'required|max:100',
            'ciudad' => 'required|max:100',
            'horario' => 'required|max:100',
            'especialidad' => 'required|max:100',
            'tipo' => 'required|max:100',
            'cpp' => 'required|max:100',
            'numero' => 'max:100'
        ];
        $this->validate($request, $rules);

        $centre = AyudaM::findOrFail($centre);

        $centre->fill($request->all());

        if($centre->isClean()){
            return $this->errorResponse(
                'Al menos un valor debe ser cambiado',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $centre->save();

        return $this->successResponse($centre);

    }

    
    /**
     * Destroy a Centre.
     *
     * @return void
     */
    public function destroy($centre)
    {
        $centre= AyudaM::findOrFail($centre);
        $centre->delete();
        return $this->successResponse($centre);
        //
    }
}
