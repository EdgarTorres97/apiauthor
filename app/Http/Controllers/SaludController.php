<?php

namespace App\Http\Controllers;

use App\Salud;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;



class SaludController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    
    /**
     * List the 
     *
     * @return void
     */
    public function index()
    {
        $saluds = Salud::all();
        return $this->successResponse($saluds);
    }

    
    /**
    
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|max:100',
            'latitud' => 'required|max:80',
            'longitud' => 'required|max:80',
            'estado' => 'required|max:100',
            'ciudad' => 'required|max:100',
            'horario' => 'required|max:100',
            'especialidad' => 'required|max:100',
            'tipo' => 'required|max:100',
            'cpp' => 'required|max:100',
            'numero' => 'required|max:100'
        ];
        $this->validate($request, $rules);

        $salud = Salud::create($request->all());

        $this->successResponse($salud, Response::HTTP_CREATED);
    }
    
    /**
     * Show an 
     *
     * @return void
     */
    public function show($salud)
    {
        $salud = Salud::findOrfail($salud);
        return $this->successResponse($salud);

    }
    
    /**
     * Update an 
     *
     * @return void
     */
    public function Update(Request $request, $salud)
    {
        $rules = [
            'nombre' => 'required|max:100',
            'latitud' => 'required|max:80',
            'longitud' => 'required|max:80',
            'estado' => 'required|max:100',
            'ciudad' => 'required|max:100',
            'horario' => 'required|max:100',
            'especialidad' => 'required|max:100',
            'tipo' => 'required|max:100',
            'cpp' => 'required|max:100',
            'numero' => 'required|max:100'
        ];
        $this->validate($request, $rules);

        $salud = Salud::findOrFail($salud);

        $salud->fill($request->all());

        if($salud->isClean()){
            return $this->errorResponse(
                'Al menos un valor debe ser cambiado',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $salud->save();

        return $this->successResponse($salud);

    }

    
    /**
     * Destroy an Author.
     *
     * @return void
     */
    public function destroy($salud)
    {
        $salud= Salud::findOrFail($salud);
        $salud->delete();
        return $this->successResponse($salud);
        //
    }
}
