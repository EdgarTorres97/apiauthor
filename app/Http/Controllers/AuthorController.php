<?php

namespace App\Http\Controllers;

use App\Author;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;



class AuthorController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    
    /**
     * List the Athors.
     *
     * @return void
     */
    public function index()
    {
        $authors = Author::all();
        return $this->successResponse($authors);
    }

    
    /**
     * Create an Author.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:100',
            'gender' => 'required|in:male,female',
            'country' => 'required|max:80'
        ];
        $this->validate($request, $rules);

        $author = Author::create($request->all());

        $this->successResponse($author, Response::HTTP_CREATED);
    }
    
    /**
     * Show an Author.
     *
     * @return void
     */
    public function show($author)
    {
        $author = Author::findOrfail($author);
        return $this->successResponse($author);

    }
    
    /**
     * Update an Author.
     *
     * @return void
     */
    public function Update(Request $request, $author)
    {
        $rules = [
            'name' => 'required|max:100',
            'gender' => 'required|in:male,female',
            'country' => 'required|max:80'
        ];
        $this->validate($request, $rules);

        $author = Author::findOrFail($author);

        $author->fill($request->all());

        if($author->isClean()){
            return $this->errorResponse(
                'Al menos un valor debe ser cambiado',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $author->save();

        return $this->successResponse($author);

    }

    
    /**
     * Destroy an Author.
     *
     * @return void
     */
    public function destroy($author)
    {
        $author= Author::findOrFail($author);
        $author->delete();
        return $this->successResponse($author);
        //
    }
}
