<?php

namespace App\Http\Controllers;

use App\Enfermedade;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;



class EnfermedadeController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    
    /**
     * List the CentreA.
     *
     * @return void
     */
    public function index()
    {
        $enfermedade = Enfermedade::all();
        return $this->successResponse($enfermedade);
    }

    
    /**
     * Create a CentreA.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|max:100',
            'latitud' => 'required|max:80',
            'longitud' => 'required|max:80',
            'estado' => 'required|max:100',
            'ciudad' => 'required|max:100',
            'horario' => 'required|max:100',
            'especialidad' => 'required|max:100',
            'tipo' => 'required|max:100',
            'cpp' => 'required|max:100',
            'numero' => 'max:100'
        ];
        $this->validate($request, $rules);

        $enfermedade = Enfermedade::create($request->all());

        $this->successResponse($enfermedade, Response::HTTP_CREATED);
    }
    
    /**
     * Show a CentreA.
     *
     * @return void
     */
    public function show($enfermedade)
    {
        $enfermedade = Enfermedade::findOrfail($enfermedade);
        return $this->successResponse($enfermedade);

    }
    
    /**
     * Update a CentreA.
     *
     * @return void
     */
    public function Update(Request $request, $enfermedade)
    {
        $rules = [
            'nombre' => 'required|max:100',
            'latitud' => 'required|max:80',
            'longitud' => 'required|max:80',
            'estado' => 'required|max:100',
            'ciudad' => 'required|max:100',
            'horario' => 'required|max:100',
            'especialidad' => 'required|max:100',
            'tipo' => 'required|max:100',
            'cpp' => 'required|max:100',
            'numero' => 'max:100'
        ];
        $this->validate($request, $rules);

        $enfermedade = Enfermedade::findOrFail($enfermedade);

        $enfermedade->fill($request->all());

        if($enfermedade->isClean()){
            return $this->errorResponse(
                'Al menos un valor debe ser cambiado',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $enfermedade->save();

        return $this->successResponse($enfermedade);

    }

    
    /**
     * Destroy a CentreA.
     *
     * @return void
     */
    public function destroy($enfermedade)
    {
        $enfermedade= Enfermedade::findOrFail($enfermedade);
        $enfermedade->delete();
        return $this->successResponse($enfermedade);
        //
    }
}
