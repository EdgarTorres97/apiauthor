<?php

namespace App\Http\Controllers;

use App\Et;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;



class EtController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    
    /**
     * List the CentreA.
     *
     * @return void
     */
    public function index()
    {
        $et = Et::all();
        return $this->successResponse($et);
    }

    
    /**
     * Create a CentreA.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|max:100',
            'latitud' => 'required|max:80',
            'longitud' => 'required|max:80',
            'estado' => 'required|max:100',
            'ciudad' => 'required|max:100',
            'horario' => 'required|max:100',
            'especialidad' => 'required|max:100',
            'tipo' => 'required|max:100',
            'cpp' => 'required|max:100',
            'numero' => 'max:100'
        ];
        $this->validate($request, $rules);

        $et = Et::create($request->all());

        $this->successResponse($et, Response::HTTP_CREATED);
    }
    
    /**
     * Show a CentreA.
     *
     * @return void
     */
    public function show($et)
    {
        $et = Et::findOrfail($et);
        return $this->successResponse($et);

    }
    
    /**
     * Update a CentreA.
     *
     * @return void
     */
    public function Update(Request $request, $et)
    {
        $rules = [
            'nombre' => 'required|max:100',
            'latitud' => 'required|max:80',
            'longitud' => 'required|max:80',
            'estado' => 'required|max:100',
            'ciudad' => 'required|max:100',
            'horario' => 'required|max:100',
            'especialidad' => 'required|max:100',
            'tipo' => 'required|max:100',
            'cpp' => 'required|max:100',
            'numero' => 'max:100'
        ];
        $this->validate($request, $rules);

        $et = Et::findOrFail($et);

        $et->fill($request->all());

        if($et->isClean()){
            return $this->errorResponse(
                'Al menos un valor debe ser cambiado',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $et->save();

        return $this->successResponse($et);

    }

    
    /**
     * Destroy a CentreA.
     *
     * @return void
     */
    public function destroy($et)
    {
        $et= Et::findOrFail($et);
        $et->delete();
        return $this->successResponse($et);
        //
    }
}
