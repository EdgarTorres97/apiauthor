<?php

namespace App\Http\Controllers;

use App\PAux;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;



class PAuxController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    
    /**
     * List the CentreA.
     *
     * @return void
     */
    public function index()
    {
        $centreA = PAux::all();
        return $this->successResponse($centreA);
    }

    
    /**
     * Create a CentreA.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|max:100',
            'latitud' => 'required|max:80',
            'longitud' => 'required|max:80',
            'estado' => 'required|max:100',
            'ciudad' => 'required|max:100',
            'horario' => 'required|max:100',
            'especialidad' => 'required|max:100',
            'tipo' => 'required|max:100',
            'cpp' => 'required|max:100',
            'numero' => 'max:100'
        ];
        $this->validate($request, $rules);

        $centreA = PAux::create($request->all());

        $this->successResponse($centreA, Response::HTTP_CREATED);
    }
    
    /**
     * Show a CentreA.
     *
     * @return void
     */
    public function show($centreA)
    {
        $centreA = PAux::findOrfail($centreA);
        return $this->successResponse($centreA);

    }
    
    /**
     * Update a CentreA.
     *
     * @return void
     */
    public function Update(Request $request, $centreA)
    {
        $rules = [
            'nombre' => 'required|max:100',
            'latitud' => 'required|max:80',
            'longitud' => 'required|max:80',
            'estado' => 'required|max:100',
            'ciudad' => 'required|max:100',
            'horario' => 'required|max:100',
            'especialidad' => 'required|max:100',
            'tipo' => 'required|max:100',
            'cpp' => 'required|max:100',
            'numero' => 'max:100'
        ];
        $this->validate($request, $rules);

        $centreA = PAux::findOrFail($centreA);

        $centreA->fill($request->all());

        if($centreA->isClean()){
            return $this->errorResponse(
                'Al menos un valor debe ser cambiado',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $centreA->save();

        return $this->successResponse($centreA);

    }

    
    /**
     * Destroy a CentreA.
     *
     * @return void
     */
    public function destroy($centreA)
    {
        $centreA= PAux::findOrFail($centreA);
        $centreA->delete();
        return $this->successResponse($centreA);
        //
    }
}
