<?php

namespace App\Http\Controllers;

use App\Persona;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;



class PersonaController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    
    /**
     * List the Persona.
     *
     * @return void
     */
    public function index()
    {
        $personas = Persona::all();
        return $this->successResponse($personas);
    }

    
    /**
     * Create an Persona.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:100',
            'gender' => 'required|in:male,female',
            'country' => 'required|max:80'
        ];
        $this->validate($request, $rules);

        $persona = Persona::create($request->all());

        $this->successResponse($persona, Response::HTTP_CREATED);
    }
    
    /**
     * Show an Persona.
     *
     * @return void
     */
    public function show($persona)
    {
        $persona = Persona::findOrfail($persona);
        return $this->successResponse($persona);

    }
    
    /**
     * Update an Persona.
     *
     * @return void
     */
    public function Update(Request $request, $persona)
    {
        $rules = [
            'name' => 'required|max:100',
            'gender' => 'required|in:male,female',
            'country' => 'required|max:80'
        ];
        $this->validate($request, $rules);

        $persona = Persona::findOrFail($persona);

        $persona->fill($request->all());

        if($persona->isClean()){
            return $this->errorResponse(
                'Al menos un valor debe ser cambiado',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $persona->save();

        return $this->successResponse($persona);

    }

    
    /**
     * Destroy an Persona.
     *
     * @return void
     */
    public function destroy($persona)
    {
        $persona= Persona::findOrFail($persona);
        $persona->delete();
        return $this->successResponse($persona);
        //
    }
}
