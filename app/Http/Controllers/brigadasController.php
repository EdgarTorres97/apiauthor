<?php

namespace App\Http\Controllers;

use App\brigadas;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;

class brigadasController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        $brigadas=brigadas::all();
        return $this->successResponse($brigadas);
    }

    public function store(Request $request){
        $rules = [
            'titulo' =>'required',
            'descripcion' =>'required',
           
        ];
    $this->validate($request,$rules);
    $brigadas= brigadas::create($request->all());

    return $this->successResponse($brigadas,Response::HTTP_CREATED);

    }

    
    public function update(Request $request, $brigadas){
        $rules= [
            'titulo' =>'required',
            'descripcion' =>'required',

        ];
        $this->validate($request,$rules);

        $brigadas = brigadas::findOrfail($brigadas);

        $brigadas->fill($request->all());
        if($brigadas->isClean()){
            return $this->errorResponse('
                Al menos un valor debe ser metido
            ', 
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    
        }
        $brigadas->save();
    return $this->successResponse($brigadas);
    }
    public function show($brigadas){
        $brigadas=brigadas::findOrfail($brigadas);
        return $this->successResponse($brigadas);
    }

    public function destroy($brigadas){
        $brigadas=brigadas::findOrfail($brigadas);
        $brigadas->delete();
        return $this->successResponse($brigadas);

    }
}
