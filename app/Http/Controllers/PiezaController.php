<?php

namespace App\Http\Controllers;

use App\Pieza;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;



class PiezaController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    
    /**
     * List the Athors.
     *
     * @return void
     */
    public function index()
    {
        $piezas = Pieza::all();
        return $this->successResponse($piezas);
    }

    
    /**
     * Create an Author.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:100',
            'price' => 'required|max:10',
            'description' => 'required|max:80'
        ];
        $this->validate($request, $rules);

        $pieza = Pieza::create($request->all());

        $this->successResponse($pieza, Response::HTTP_CREATED);
    }
    
    /**
     * Show an Author.
     *
     * @return void
     */
    public function show($pieza)
    {
        $pieza = Pieza::findOrfail($pieza);
        return $this->successResponse($pieza);

    }
    
    /**
     * Update an Author.
     *
     * @return void
     */
    public function Update(Request $request, $pieza)
    {
        $rules = [
            'name' => 'required|max:100',
            'price' => 'required|max:10',
            'description' => 'required|max:200'
        ];
        $this->validate($request, $rules);

        $pieza = Pieza::findOrFail($pieza);

        $pieza->fill($request->all());

        if($pieza->isClean()){
            return $this->errorResponse(
                'Al menos un valor debe ser cambiado',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $pieza->save();

        return $this->successResponse($pieza);

    }

    
    /**
     * Destroy an Author.
     *
     * @return void
     */
    public function destroy($pieza)
    {
        $pieza= Pieza::findOrFail($pieza);
        $pieza->delete();
        return $this->successResponse($pieza);
        //
    }
}
