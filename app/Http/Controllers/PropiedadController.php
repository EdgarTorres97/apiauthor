<?php

namespace App\Http\Controllers;

use App\Propiedad;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;



class PropiedadController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    
    /**
     * List the Athors.
     *
     * @return void
     */
    public function index()
    {
        $propiedads = Propiedad::all();
        return $this->successResponse($propiedads);
    }

    
    /**
     * Create an Author.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:100',
            'city' => 'required|max:100',
            'country' => 'required|max:80'
        ];
        $this->validate($request, $rules);

        $propiedad = Propiedad::create($request->all());

        $this->successResponse($propiedad, Response::HTTP_CREATED);
    }
    
    /**
     * Show an Author.
     *
     * @return void
     */
    public function show($propiedad)
    {
        $propiedad = Propiedad::findOrfail($propiedad);
        return $this->successResponse($propiedad);

    }
    
    /**
     * Update an Author.
     *
     * @return void
     */
    public function Update(Request $request, $propiedad)
    {
        $rules = [
            'name' => 'required|max:100',
            'city' => 'required|max:100',
            'country' => 'required|max:80'
        ];
        $this->validate($request, $rules);

        $propiedad = Propiedad::findOrFail($propiedad);

        $propiedad->fill($request->all());

        if($propiedad->isClean()){
            return $this->errorResponse(
                'Al menos un valor debe ser cambiado',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $propiedad->save();

        return $this->successResponse($propiedad);

    }

    
    /**
     * Destroy an Author.
     *
     * @return void
     */
    public function destroy($propiedad)
    {
        $propiedad= Propiedad::findOrFail($propiedad);
        $propiedad->delete();
        return $this->successResponse($propiedad);
        //
    }
}
