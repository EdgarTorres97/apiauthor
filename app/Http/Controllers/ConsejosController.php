<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use App\Consejos;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\ApiResponser;


class ConsejosController extends Controller
{
    use ApiResponser;   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index(){
        $consejo = Consejos::all();
        return $this->successResponse($consejo);
    }
    public function store(Request $request){
        $rules = [
            'titulo' => 'required|max:255',
            'descripcion' => 'required|max:255',
            
        ];

        $this->validate($request->all(), $rules);

        $consejo = Consejos::create($request->all());
        
        return $this->successResponse($consejo, Response::HTTP_CREATED);
    }

    public function update(Request $request, $consejo){
        $rules = [
            'nombre' => 'required|max:255',
            'description' => 'required|max:255',
            
        ];

        $this->validate($request->all(), $rules);

        $consejo = Consejos::findOrFail($consejo);

        $consejo->fill($request->all());

        if($consejo->isClean()){
            return $this->errorResponse('At least one value must be changed', Response::HTTP_UNPROCESSABLE_ENTITY);            
        }
        
        $consejo->save();

        return $this->successResponse($consejo);
    }

    public function destroy($consejo){
        $consejo = Consejos::findOrFail($consejo);
        $consejo->delete();
        return $this->successResponse($consejo);
    }

    //
}