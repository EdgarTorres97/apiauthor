<?php

namespace App\Http\Controllers;

use App\Pelicula;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;



class PeliculaController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    
    /**
     * List the Athors.
     *
     * @return void
     */
    public function index()
    {
        $peliculas = Pelicula::all();
        return $this->successResponse($peliculas);
    }

    
    /**
     * Create an Author.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:100',
            'gender' => 'required|in:horror,action,sad',
            'description' => 'required|max:80'
        ];
        $this->validate($request, $rules);

        $pelicula = Pelicula::create($request->all());

        $this->successResponse($pelicula, Response::HTTP_CREATED);
    }
    
    /**
     * Show an Author.
     *
     * @return void
     */
    public function show($pelicula)
    {
        $pelicula = Pelicula::findOrfail($pelicula);
        return $this->successResponse($pelicula);

    }
    
    /**
     * Update an Author.
     *
     * @return void
     */
    public function Update(Request $request, $pelicula)
    {
        $rules = [
            'name' => 'required|max:100',
            'gender' => 'required|in:horror,action,sad',
            'description' => 'required|max:200'
        ];
        $this->validate($request, $rules);

        $pelicula = Pelicula::findOrFail($pelicula);

        $pelicula->fill($request->all());

        if($pelicula->isClean()){
            return $this->errorResponse(
                'Al menos un valor debe ser cambiado',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $pelicula->save();

        return $this->successResponse($pelicula);

    }

    
    /**
     * Destroy an Author.
     *
     * @return void
     */
    public function destroy($pelicula)
    {
        $pelicula= Pelicula::findOrFail($pelicula);
        $pelicula->delete();
        return $this->successResponse($pelicula);
        //
    }
}
