<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class brigadas extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo', 
        'descripcion',
        
    ];

}
