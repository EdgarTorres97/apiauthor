<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PAux extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'latitud',
        'longitud',
        'estado',
        'ciudad',
        'horario',
        'especialidad',
        'tipo',
        'cpp',
        'numero'
    ];
}
