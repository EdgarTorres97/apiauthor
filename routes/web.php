<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|Aquí es donde tu puedes regitrar todas las rutas de una aplicación
*/
// 


$router->get('/personas', 'PersonaController@index');
$router->post('/personas', 'PersonaController@store');
$router->get('/personas/{persona}', 'PersonaController@show');
$router->put('/personas/{persona}', 'PersonaController@update');
$router->delete('/personas/{persona}', 'PersonaController@destroy');

$router->get('/propiedads', 'PropiedadController@index');
$router->post('/propiedads', 'PropiedadController@store');
$router->get('/propiedads/{propiedads}', 'PropiedadController@show');
$router->put('/propiedads/{propiedads}', 'PropiedadController@update');
$router->delete('/propiedads/{propiedads}', 'PropiedadController@destroy');

$router->get('/peliculas', 'PeliculaController@index');
$router->post('/peliculas', 'PeliculaController@store');
$router->get('/peliculas/{peliculas}', 'PeliculaController@show');
$router->put('/peliculas/{peliculas}', 'PeliculaController@update');
$router->delete('/peliculas/{peliculas}', 'PeliculaController@destroy');

$router->get('/piezas', 'PiezaController@index');
$router->post('/piezas', 'PiezaController@store');
$router->get('/piezas/{piezas}', 'PiezaController@show');
$router->put('/piezas/{piezas}', 'PiezaController@update');
$router->delete('/piezas/{piezas}', 'PiezaController@destroy');






$router->get('/api',['middleware'=>'basic','uses'=>'ApiController@index']);

Route::group(['middleware' => 'basic'], function () {
    Route::get('/authors', 'AuthorController@index');
    Route::post('/authors', 'AuthorController@store');
    Route::get('/authors/{author}', 'AuthorController@show');
    Route::put('/authors/{author}', 'AuthorController@update');
    Route::delete('/authors/{author}', 'AuthorController@destroy');

    Route::get('/enfermedades', 'EnfermedadeController@index');
    Route::post('/enfermedades', 'EnfermedadeController@store');
    Route::get('/enfermedades/{enfermedades}', 'EnfermedadeController@show');
    Route::put('/enfermedades/{enfermedades}', 'EnfermedadeController@update');
    Route::delete('/enfermedades/{enfermedades}', 'EnfermedadeController@destroy');

    Route::get('/saluds', 'SaludController@index');
    Route::post('/saluds', 'SaludController@store');
    Route::get('/saluds/{saluds}', 'SaludController@show');
    Route::put('/saluds/{saluds}', 'SaludController@update');
    Route::delete('/saluds/{saluds}', 'SaludController@destroy');

    Route::get('/ayudaM', 'AyudaMController@index');
    Route::post('/ayudaM', 'AyudaMController@store');
    Route::get('/ayudaM/{centre}', 'AyudaMController@show');
    Route::put('/ayudaM/{centre}', 'AyudaMController@update');
    Route::delete('/ayudaM/{centre}', 'AyudaMController@destroy');

    Route::get('/paux', 'PAuxController@index');
    Route::post('/paux', 'PAuxController@store');
    Route::get('/paux/{centreA}', 'PAuxController@show');
    Route::put('/paux/{centreA}', 'PAuxController@update');
    Route::delete('/paux/{centreA}', 'PAuxController@destroy');

    Route::get('/ets', 'EtController@index');
    Route::post('/ets', 'EtController@store');
    Route::get('/ets/{ets}', 'EtController@show');
    Route::put('/ets/{ets}', 'EtController@update');
    Route::delete('/ets/{ets}', 'EtController@destroy');

    Route::get('/plantas','brigadasController@index');
    Route::post('/plantas','brigadasController@store');
    Route::get('/plantas/{author}','brigadasController@show');
    Route::put('/plantas/{author}','brigadasController@update');
    Route::delete('/plantas/{author}','brigadasController@destroy');

    Route::get('/consejos', 'ConsejosController@index');
    Route::post('/consejos', 'ConsejosController@store');
    Route::get('/consejos/{consejos}', 'ConsejosController@show');
    Route::put('/consejos/{consejos}', 'ConsejosController@update');
    Route::patch('/consejos/{consejos}', 'ConsejosController@update');
    Route::delete('/consejos/{consejos}', 'ConsejosController@destroy');
});

use Illuminate\Support\Facades\Artisan;
Route::get('/comandos', function () {
    $exitCode = Artisan::call('migrate:refresh --seed'
    );
});
