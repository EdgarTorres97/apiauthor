<?php

use App\Author;
use App\Persona;
use App\Propiedad;
use App\Pelicula;
use App\Pieza;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Author::class, 50)->create();
        factory(Persona::class, 50)->create();
        factory(Propiedad::class, 50)->create();
        factory(Pelicula::class, 50)->create();
        factory(Pieza::class, 50)->create();

        DB::table('saluds')->insert([
            'nombre' => 'Urgencias Hospital Regional No. 17',
            'latitud' => 21.135661,
            'longitud' => -86.854522,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 horas',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77533,
            'numero'=> '998 267 8705',

        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Unidad De Medicina Familiar  No.13',
            'latitud' => 21.196383,
            'longitud' => -86.876372,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 horas',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77516,
            'numero'=> '998 234 0000',

        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Hospital General de Cancún "Jesús Kumate Rodríguez',
            'latitud' => 21.196213,
            'longitud' => -86.876361,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 horas',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77524,
            'numero'=> '(998) 88 429 67',

        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Urbano No. 1 Región 100',
            'latitud' => 21.152746,
            'longitud' => -86.873227,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' 07:00:00-19:30:00',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77518,
            'numero'=> '(998) 88 395 81',
        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Urbano No. 2 La Cuchilla',
            'latitud' => 21.180955,
            'longitud' => -86.830407,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' 07:00:00-15:00:00',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77527,
            'numero'=> '(998) 98 987 53',
        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Urbano No. 3 Región 95',
            'latitud' => 21.146038,
            'longitud' => -86.858946,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' 07:00:00-15:00:00',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77534,
            'numero'=> '(998) 20 687 75',
        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Urbano No. 4 Región 96',
            'latitud' => 21.153363,
            'longitud' => -86.880999,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' 07:00:00-17:00:00',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77538,
            'numero'=> '(998) 28 927 82',
        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Urbano No. 6 Región 99',
            'latitud' => 21.142641,
            'longitud' => -86.87396,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' 07:00:00-20:00:00',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77535,
            'numero'=> '(998) 84 363 95',
        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Urbano No. 7 Región 102',
            'latitud' => 21.143057,
            'longitud' => -86.880091,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' 08:00:00-13:30:00',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77535,
            'numero'=> '(998) 84 366 35',
        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Urbano No. 9 Región 85',
            'latitud' => 21.183232,
            'longitud' => -86.809711,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' ',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77525,
            'numero'=> '(998) 84 310 21',
        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Urbano No. 10 Región 229',
            'latitud' => 21.174394,
            'longitud' => -86.858977,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' ',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77516,
            'numero'=> '(998) 13 289 17',
        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Urbano No.11 Región 233',
            'latitud' => 21.186237,
            'longitud' => -86.841228,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' 07:00:00-20:00:00',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77510,
            'numero'=> '(998) 86 005 20',
        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Urbano No.12 Región 236',
            'latitud' => 21.194573,
            'longitud' => -86.833593,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' 09:00:00-17:00:00',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77535,
            'numero'=> '(998) 88 287 05',
        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Urbano No. 14 Región 221',
            'latitud' => 21.174872,
            'longitud' => -86.865388,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' ',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77517,
            'numero'=> '(998) 19 593 35',
        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Urbano No.15 Región 103',
            'latitud' => 21.1488,
            'longitud' => -86.892903,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' ',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77539,
            'numero'=> '(998) 21 587 17',
        ]);
        DB::table('saluds')->insert([
            'nombre' => 'Centro de Salud Rural Alfredo V. Bonfil',
            'latitud' => 21.087629,
            'longitud' => -86.854488,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> ' 09:00:00-17:00:00',
            'especialidad'=> 'General',
            'tipo'=> 'IMSS',
            'cpp'=> 77536,
            'numero'=> '(998) 88 226 46',
        ]);

        DB::table('p_auxes')->insert([
            'nombre' => 'Secretaría General del H. Ayuntamiento',
            'latitud' => 21.161422,
            'longitud' => -86.82478,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> '9:00:00 AM-17:00:00 p.m.',
            'especialidad'=> 'General',
            'tipo'=> 'Precencial',
            'cpp'=> 77500,
            'numero'=> '8 84 12 02 Ext.',
        ]);
        DB::table('p_auxes')->insert([
            'nombre' => 'AquaWorld',
            'latitud' => 21.15046,
            'longitud' => -86.793276,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> '8:00:00 AM	20:00:00 p.m.',
            'especialidad'=> 'General',
            'tipo'=> 'Precencial',
            'cpp'=> 77500,
            'numero'=> ''
        ]);
        DB::table('p_auxes')->insert([
            'nombre' => 'Cruz Roja Mexicana Delegación Cancún',
            'latitud' => 21.155306,
            'longitud' => -86.829858,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> '8:00:00 AM	20:00:00 p.m.',
            'especialidad'=> 'General',
            'tipo'=> 'Precencial',
            'cpp'=> 77500,
            'numero'=> 'Tel. directo: 884-16-16Tel. de emergencias: 911'
        ]);
        DB::table('p_auxes')->insert([
            'nombre' => 'Heróico Cuerpo de Bomberos Cancún',
            'latitud' => 21.169406,
            'longitud' => -86.826826,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Siempre abierto	',
            'especialidad'=> 'General',
            'tipo'=> 'Precencial',
            'cpp'=> 77500,
            'numero'=> '998 884 1202'
        ]);
        DB::table('p_auxes')->insert([
            'nombre' => 'Cursos de Capacitación',
            'latitud' => 19.388115,
            'longitud' => -99.181837,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'De 6 a 8 horas dura el curso	',
            'especialidad'=> 'General',
            'tipo'=> 'Precencial',
            'cpp'=> 77500,
            'numero'=> '9982912409'
        ]);
        DB::table('p_auxes')->insert([
            'nombre' => 'Grupo Sardex',
            'latitud' => 21.14076,
            'longitud' => -86.850248,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> '8:00:00 AM	18:00:00 p.m.',
            'especialidad'=> 'General',
            'tipo'=> 'Precencial',
            'cpp'=> 77500,
            'numero'=> '01 (998) 884 5031 Y 884 5034​'
        ]);
    
        DB::table('ayuda_m_s')->insert([
            'nombre' => 'UNEME DEDICAM',
            'latitud' => 21.1980463,
            'longitud' => -86.876632,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> '7:00	22:00',
            'especialidad'=> 'Centro de apoyo a la mujer',
            'tipo'=> 'UNEME',
            'cpp'=> 00000,
            'numero'=> '998 568 0424​'
        ]);
        DB::table('ayuda_m_s')->insert([
            'nombre' => 'Centro Integral de Atención a las Mujeres CIAM',
            'latitud' => 21.14883,
            'longitud' => -86.891997,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> '9:00:00 AM	17:00:00 p.m.',
            'especialidad'=> 'Centro de apoyo a la mujer',
            'tipo'=> 'CIAM',
            'cpp'=> 77539,
            'numero'=> '998 898 0755​'
        ]);
        DB::table('ayuda_m_s')->insert([
            'nombre' => 'INSTITUTO MUNICIPAL DE LA MUJER',
            'latitud' => 21.150748,
            'longitud' => -86.867108,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'N/A',
            'especialidad'=> 'INSTITUTO MUNICIPAL DE LA MUJER',
            'tipo'=> 'INSTITUTO MUNICIPAL DE LA MUJER',
            'cpp'=> 77516,
            'numero'=> ''
        ]);
        DB::table('ayuda_m_s')->insert([
            'nombre' => 'Centro de Justicia para las Mujeres',
            'latitud' => 21.162898,
            'longitud' => -86.86337,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'N/A',
            'especialidad'=> 'Centro de Justicia para las Mujeres',
            'tipo'=> 'Centro de Justicia para las Mujeres',
            'cpp'=> 77517,
            'numero'=> ''
        ]);

        //tabla ets
        DB::table('ets')->insert([
            'nombre' => 'UNEME CAPASITS cancun',
            'latitud' => 21.177022,
            'longitud' => -86.86337,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> '07:00-15:00',
            'especialidad'=> 'Experto ETS',
            'tipo'=> 'Tratamiento',
            'cpp'=> 77500,
            'numero'=> ''
        ]);

        DB::table('ets')->insert([
            'nombre' => 'Hospital San Gabriel',
            'latitud' => 21.141056,
            'longitud' => -86.854662,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 horas',
            'especialidad'=> 'Experto ETS',
            'tipo'=> 'Tratamiento',
            'cpp'=> 77500,
            'numero'=> '998 848 3836'
        ]);

        DB::table('ets')->insert([
            'nombre' => 'Urología Cancún',
            'latitud' => 21.158255,
            'longitud' => -86.828056,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> '08:00-20:00',
            'especialidad'=> 'Experto ETS',
            'tipo'=> 'Tratamiento',
            'cpp'=> 77500,
            'numero'=> '998 884 1419 ext. 300'
        ]);

        DB::table('ets')->insert([
            'nombre' => 'Hospital Victoria',
            'latitud' => 21.162195,
            'longitud' => -86.830373,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 Horas',
            'especialidad'=> 'Experto ETS',
            'tipo'=> 'Tratamiento',
            'cpp'=> 77500,
            'numero'=> '998 887 6206'
        ]);

        //tabla enfermedades Basicas
        DB::table('enfermedades')->insert([
            'nombre' => 'Hospital ISSSTE cancun',
            'latitud' => 21.165085,
            'longitud' => -86.856316,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 Horas',
            'especialidad'=> 'General',
            'tipo'=> 'ISSTE',
            'cpp'=> 77500,
            'numero'=> '998 888 7261'
        ]);

        DB::table('enfermedades')->insert([
            'nombre' => 'Hospital Amerimed Cancun, S.A de C.V',
            'latitud' => 21.14453,
            'longitud' => -86.822394,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 Horas',
            'especialidad'=> 'General',
            'tipo'=> 'Privado',
            'cpp'=> 77500,
            'numero'=> '998 881 3400'
        ]);

        DB::table('enfermedades')->insert([
            'nombre' => 'Hospital Galenia',
            'latitud' => 21.136343,
            'longitud' => -86.827401,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 Horas',
            'especialidad'=> 'General',
            'tipo'=> 'Privado',
            'cpp'=> 77500,
            'numero'=> '998 891 5200'
        ]);

        DB::table('enfermedades')->insert([
            'nombre' => 'Hospital Playamed Cancun',
            'latitud' => 21.164361,
            'longitud' => -86.823999,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 Horas',
            'especialidad'=> 'General',
            'tipo'=> 'Privado',
            'cpp'=> 77509,
            'numero'=> '998 140 5258'
        ]);

        DB::table('enfermedades')->insert([
            'nombre' => 'Hospiten Cancún',
            'latitud' => 21.13827,
            'longitud' => -86.823511,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 Horas',
            'especialidad'=> 'General',
            'tipo'=> 'Privado',
            'cpp'=> 77500,
            'numero'=> '998 881 3700'
        ]);

        DB::table('enfermedades')->insert([
            'nombre' => 'Medcal',
            'latitud' => 21.138835,
            'longitud' => -86.869466,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 Horas',
            'especialidad'=> 'General',
            'tipo'=> 'Privado',
            'cpp'=> 77500,
            'numero'=> '998 254 3972'
        ]);

        DB::table('enfermedades')->insert([
            'nombre' => 'Hospital Quirúrgica del Sur',
            'latitud' => 21.158694,
            'longitud' => -86.857274,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 Horas',
            'especialidad'=> 'General',
            'tipo'=> 'Privado',
            'cpp'=> 77500,
            'numero'=> '998 843 5454'
        ]);

        DB::table('enfermedades')->insert([
            'nombre' => 'Hospital Americano',
            'latitud' => 21.152236,
            'longitud' => -86.823564,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 Horas',
            'especialidad'=> 'General',
            'tipo'=> 'Privado',
            'cpp'=> 77500,
            'numero'=> '998 287 8023'
        ]);

        DB::table('enfermedades')->insert([
            'nombre' => 'Cancun Oncology Center',
            'latitud' => 21.136797,
            'longitud' => -86.827233,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 Horas',
            'especialidad'=> 'General',
            'tipo'=> 'Privado',
            'cpp'=> 77504,
            'numero'=> '998 891 5205'
        ]);

        DB::table('enfermedades')->insert([
            'nombre' => 'MEDITRAVEL CENTER',
            'latitud' => 21.134222,
            'longitud' => -86.747928,
            'estado'=> 'Quintana Roo',
            'ciudad'=> 'Cancún',
            'horario'=> 'Abierto las 24 Horas',
            'especialidad'=> 'General',
            'tipo'=> 'Privado',
            'cpp'=> 77504,
            'numero'=> '55 4610 2530'
        ]);

        DB::table('brigadas')->insert([
            'titulo' => 'Recoleccion de sargazo',
            'descripcion' => 'Se recogera sargazo en las siguientes playas:
            Playa delfines, playa tortugas',
            'FechaEvent'=>'10/11/2019'
        ]);
        DB::table('brigadas')->insert([
            'titulo' => 'Recollecion de Basura Parque Xaab',
            'descripcion' => 'En el parque ubicado:228 mz 31 lt 40 se llevara acabo la recoleccion de basura. ',
            'FechaEvent'=>'10/11/2019'
        ]);

        //
        DB::table('brigadas')->insert([
            'titulo'=>'Primera brigada de limpieza.',
            'descripcion'=>'Ven y se parte de esta gran campaña, que tendrá como fin ayudar a nuestro planeta,
            en el cual se tendrán grandes recompenzas.',
            'FechaEvent'=>'30/11/2019'
        ]);
        DB::table('brigadas')->insert([
            'titulo'=>'Campaña de reforestación',
            'descripcion'=>'Se realizara una brigada para plantar arboles en puntos estrategicos en la zona centrica de cancun. Jimmy Ek' ,
            'FechaEvent'=>'1 de diciembre al 3 diciembre del 2019'
        ]);
        DB::table('brigadas')->insert([
            'titulo'=>'Campaña de reforestación',
            'descripcion'=>'¡XAAB te invita a que formes parte de la segunda campaña de reforestación!... Ven y ayudanos a salvar el planeta',
            'FechaEvent'=>'15/12/2019'
        ]);
        DB::table('brigadas')->insert([
            'titulo'=>'Campaña de donación',
            'descripcion'=>'Recuerda que si te sobra alguna planta puedes donarlo para algún lugar que lo requiera... Ven y ayudanos a reforestar el planeta',
            'FechaEvent'=>'27/12/2019'
        ]);
        DB::table('brigadas')->insert([
            'titulo'=>'Limpieza de parques',
            'descripcion'=>'Porque no solo es importante plantar arboles, también es importante cuidarlos... Ven y ayudanos a la primera limpieza de parques, ¡Acompañanos!',
            'FechaEvent'=>'10/01/2020'
        ]);




        // Esto es de andres CONSEJOS
        DB::table('consejos')->insert([
            'titulo'=>'Uso de jabones neutros para zonas intimas',
            'descripcion'=>'Este evitara infecciones por hongos al ser, zonas sensibles a la era  nuestras partes intimas con jabones con colorantes y perfumes que podrían ocasionar problemas en nuestra zonaa más sensibles'
        ]);
        DB::table('consejos')->insert([
            'titulo'=>'No compartir toallas',
            'descripcion'=>'Debemos tener claro que el compartir de "nuestros germenes" no es lo mas adecuado, ya que cada quien posee un sistema inmunologico distinto, y por ende, es mas resistente a unas cosas que tal vez los demás no son.'
        ]);
        DB::table('consejos')->insert([
            'titulo'=>'Depilacion intima: como calmar la sensibilizacion.',
            'descripcion'=>'Es importante  tener una higiene intima adecuada antes y despues de la depilacion, y utilizar una crema o gel calmante después de depilarse para ayudar a aliviar la sensacion de irritacion y  aportar frescor y alivio a la zona depilada.'
        ]);

    }
}
