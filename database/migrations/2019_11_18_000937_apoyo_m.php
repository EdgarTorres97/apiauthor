<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ApoyoM extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ayuda_m_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',100);
            $table->double('latitud', 10,8);
            $table->double('longitud', 10,8);
            $table->string('estado');
            $table->string('ciudad');
            $table->string('horario');
            $table->string('especialidad');
            $table->string('tipo');
            $table->integer('cpp');
            $table->string('numero');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ayuda_m_s');
    }
}
