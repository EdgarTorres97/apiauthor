<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Author::class, function (Faker\Generator $faker) {
    return [
        'gender' => $gender = $faker->randomElement(['male','female']),
        'name' => $faker->name($gender),
        'country' => $faker->country
    ];
});
$factory->define(App\Persona::class, function (Faker\Generator $faker) {
    return [
        'gender' => $gender = $faker->randomElement(['male','female']),
        'name' => $faker->name($gender),
        'country' => $faker->country
    ];
});
$factory->define(App\Propiedad::class, function (Faker\Generator $faker) {
    return [
        'city' => $faker->city,
        'name' => $faker->name,
        'country' => $faker->country
    ];
});
$factory->define(App\Pelicula::class, function (Faker\Generator $faker) {
    return [
        'gender' => $gender = $faker->randomElement(['horror','action','sad']),
        'name' => $faker->name,
        'description' => $description = $faker->randomElement(['pelicula fea','pelicula buena','pelicula excelente'])
    ];
});

$factory->define(App\Pieza::class, function (Faker\Generator $faker) {
    return [
        'price' => $price = $faker->randomElement(['mil','dosmil','tresmil']),
        'name' => $faker->name,
        'description' => $description = $faker->randomElement(['Ram','tarjeta madre','tarjeta de video'])
    ];
});